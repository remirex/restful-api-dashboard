<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('id', 1)->first();

        $user = factory(User::class)->create([
            'name' => 'Super Admin',
            'email' => 'admin@admin.test',
            'password' => bcrypt('admin1234')
        ]);

        $user->assignRole($role);
    }
}
