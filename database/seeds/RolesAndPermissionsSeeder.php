<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'create articles', 'guard_name' => 'api']);
        Permission::create(['name' => 'edit articles', 'guard_name' => 'api']);
        Permission::create(['name' => 'delete articles', 'guard_name' => 'api']);
        Permission::create(['name' => 'publish articles', 'guard_name' => 'api']);
        Permission::create(['name' => 'unpublish articles', 'guard_name' => 'api']);

        // create roles and assign created permissions

        $role = Role::create(['name' => 'super-admin', 'guard_name' => 'api']);
        $role->givePermissionTo(Permission::all());
    }
}
