<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BrandRepository.
 *
 * @package namespace App\Repositories;
 */
interface BrandRepository extends RepositoryInterface
{
    /**
     * @param string $order
     * @param string $sort
     * @return mixed
     */
    public function listBrands(string $order = 'id', string $sort = 'desc');

    /**
     * @param int $id
     * @return mixed
     */
    public function findBrandById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createBrand(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateBrand(array $params);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteBrand($id);
}
