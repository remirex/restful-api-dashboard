<?php

namespace App\Repositories;

use App\Http\Resources\Roles;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RoleRepository;
use Spatie\Permission\Models\Role;
use App\Validators\RoleValidator;

/**
 * Class RoleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param string $order
     * @param string $sort
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function roles(string $order = 'id', string $sort = 'desc')
    {
        // TODO: Implement roles() method.
        $roles = $this->model->orderBy($order, $sort)->get();

        return Roles::collection($roles);
    }

    public function findRoleById(int $id)
    {
        // TODO: Implement findRoleById() method.
        $role = $this->model->findOrFail($id);

        return new Roles($role);
    }

    /**
     * @param array $params
     * @return Roles|mixed
     */
    public function createRole(array $params)
    {
        // TODO: Implement createRole() method.
        $collection = collect($params)->except('permission');

        $role = new Role($collection->all());

        $role->save();

        $permissions = $params['permission'];

        if (isset($permissions)) {
            $role->givePermissionTo($permissions);
        }

        return new Roles($role);
    }

    /**
     * @param array $params
     * @return Roles|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateRole(array $params)
    {
        // TODO: Implement updateRole() method.
        $role = $this->findRoleById($params['id']);

        $collection = collect($params)->except('permission');

        $permissions = $params['permission'];

        if (isset($permissions)) {
            $role->syncPermissions($permissions);
        }

        $role = $this->update($collection->all(), $role->id);

        return new Roles($role);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function deleteRole($id)
    {
        // TODO: Implement deleteRole() method.
        $role = $this->findRoleById($id);

        $role->delete($role->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted role!'
        ]);
    }
}
