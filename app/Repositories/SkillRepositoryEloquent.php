<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SkillRepository;
use App\Models\Skill;
use App\Validators\SkillValidator;

/**
 * Class SkillRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SkillRepositoryEloquent extends BaseRepository implements SkillRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Skill::class;
    }

    /**
     * Get Model object
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function skills()
    {
        return $this->model->all();
    }
    
}
