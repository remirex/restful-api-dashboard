<?php

namespace App\Repositories;

use App\Http\Resources\Permissions;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PermissionRepository;
use App\Validators\PermissionValidator;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PermissionRepositoryEloquent extends BaseRepository implements PermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param string $order
     * @param string $sort
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function permissions(string $order = 'id', string $sort = 'desc')
    {
        // TODO: Implement permissions() method.
        $permissions = $this->model->orderBy($order, $sort)->get();

        return Permissions::collection($permissions);
    }

    /**
     * @param int $id
     * @return Permissions|mixed
     */
    public function findPermissionById(int $id)
    {
        // TODO: Implement findPermissionById() method.
        $permission = $this->model->findOrFail($id);

        return new Permissions($permission);
    }

    /**
     * @param array $params
     * @return Permissions|mixed
     */
    public function createPermission(array $params)
    {
        // TODO: Implement createPermission() method.
        $collection = collect($params);

        $permission = new Permission($collection->all());

        $permission->save();

        return new Permissions($permission);
    }

    /**
     * @param array $params
     * @return Permissions|mixed
     */
    public function updatePermission(array $params)
    {
        // TODO: Implement updatePermission() method.
        $permission = $this->findPermissionById($params['id']);

        $collection = collect($params);

        $permission = $this->update($collection->all(), $permission->id);

        return new Permissions($permission);
    }

    /**
     * @param $permissionId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function deletePermission($permissionId)
    {
        // TODO: Implement deletePermission() method.
        $permission = $this->findPermissionById($permissionId);

        $permission->delete($permission->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted permission!'
        ]);
    }
}
