<?php

namespace App\Repositories;

use App\Http\Resources\Attributes;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AttributeRepository;
use App\Models\Attribute;
use App\Validators\AttributeValidator;

/**
 * Class AttributeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AttributeRepositoryEloquent extends BaseRepository implements AttributeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Attribute::class;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param string $order
     * @param string $sort
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function listAttributes(string $order = 'id', string $sort = 'desc')
    {
        // TODO: Implement listAttributes() method.
        $listAttributes = $this->model->orderBy($order, $sort)->get();

        return Attributes::collection($listAttributes);
    }

    /**
     * @param int $id
     * @return Attributes|mixed
     */
    public function findAttributeById(int $id)
    {
        // TODO: Implement findAttribute() method.
        $findAttribute = $this->model->findOrFail($id);

        return new Attributes($findAttribute);
    }

    /**
     * @param array $params
     * @return Attributes|mixed
     */
    public function createAttribute(array $params)
    {
        // TODO: Implement createAttribute() method.
        $collection = collect($params);

        $is_filterable = $collection->has('is_filterable') ? 1 : 0;
        $is_required = $collection->has('is_required') ? 1 : 0;

        $merge = $collection->merge(compact('is_filterable', 'is_required'));

        $attribute = new Attribute($merge->all());

        $attribute->save();

        return new Attributes($attribute);
    }

    /**
     * @param array $params
     * @return Attributes|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateAttribute(array $params)
    {
        // TODO: Implement updateAttribute() method.
        $attribute = $this->findAttributeById($params['id']);

        $collection = collect($params);

        $is_filterable = $collection->has('is_filterable') ? 1 : 0;
        $is_required = $collection->has('is_required') ? 1 : 0;

        $merge = $collection->merge(compact('is_filterable', 'is_required'));

        $attribute = $this->update($merge->all(), $attribute->id);

        return new Attributes($attribute);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function deleteAttribute($id)
    {
        // TODO: Implement deleteAttribute() method.
        $attribute = $this->findAttributeById($id);

        $this->delete($attribute->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted attribute!'
        ]);
    }

}
