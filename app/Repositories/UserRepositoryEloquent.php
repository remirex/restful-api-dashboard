<?php

namespace App\Repositories;

use App\Http\Resources\Users;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\User;
use App\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get Model object
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function listUsers(string $order = 'id', string $sort = 'desc')
    {
        // TODO: Implement listUsers() method.
        $users = $this->model->orderBy($order, $sort)->get();

        return Users::collection($users);
    }

    public function details()
    {
        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->with(['skills', 'roles'])->first();

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }
}
