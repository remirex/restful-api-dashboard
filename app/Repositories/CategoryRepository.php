<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{
    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listCategories(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param int $id
     * @return mixed
     */
    public function findCategoryById(int $id);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createCategory(array $attributes);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function updateCategory(array $attributes);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function deleteCategory($id);
}
