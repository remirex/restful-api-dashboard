<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRepository.
 *
 * @package namespace App\Repositories;
 */
interface PermissionRepository extends RepositoryInterface
{
    /**
     * @param string $order
     * @param string $sort
     * @return mixed
     */
    public function permissions(string $order = 'id', string $sort = 'desc');

    /**
     * @param int $id
     * @return mixed
     */
    public function findPermissionById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createPermission(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updatePermission(array $params);

    /**
     * @param $permissionId
     * @return mixed
     */
    public function deletePermission($permissionId);
}
