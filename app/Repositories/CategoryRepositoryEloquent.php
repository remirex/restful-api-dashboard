<?php

namespace App\Repositories;

use App\Http\Resources\Categories;
use App\Traits\UploadTrait;
use Illuminate\Http\UploadedFile;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Category;

/**
 * Class CategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CategoryRepositoryEloquent extends BaseRepository implements CategoryRepository
{
    use UploadTrait;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * Get Model object
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function listCategories(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        // TODO: Implement listCategories() method.
        $listCategories = $this->model->orderBy($order, $sort)->get();

        return Categories::collection($listCategories);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function findCategoryById(int $id)
    {
        // TODO: Implement findCategoryById() method.
        $data = $this->model->findOrFail($id);

        return new Categories($data);
    }

    /**
     * @param array $attributes
     * @return Category|mixed
     */
    public function createCategory(array $attributes)
    {
        // TODO: Implement createCategory() method.
        $collection = collect($attributes);

        $image = null;

        if ($collection->has('image') && ($attributes['image'] instanceof UploadedFile)) {
            $image = $this->uploadFile($attributes['image'], 'categories');
        }

        $featured = $collection->has('featured') ? 1 : 0;
        $menu = $collection->has('menu') ? 1 : 0;

        $merge = $collection->merge(compact('featured','menu', 'image'));

        $category = new Category($merge->all());

        $category->save();

        return new Categories($category);
    }

    /**
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function updateCategory(array $attributes)
    {
        // TODO: Implement updateCategory() method.
        $category = $this->findCategoryById($attributes['id']);

        $collection = collect($attributes);

        if ($collection->has('image') && ($attributes['image'] instanceof UploadedFile)) {
            if ($category->image != null) {
                $this->unlinkFile($category->image, 'categories');
            }

            $image = $this->uploadFile($attributes['image'], 'categories');
        }

        $featured = $collection->has('featured') ? 1 : 0;
        $menu = $collection->has('menu') ? 1 : 0;

        $merge = $collection->merge(compact('menu', 'image', 'featured'));

        $category = $this->update($merge->all(), $category->id);

        return new Categories($category);
    }

    /**
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse|int|mixed
     */
    public function deleteCategory($id)
    {
        // TODO: Implement deleteCategory() method.
        $category = $this->findCategoryById($id);

        if ($category->image != null) {
            $this->unlinkFile($category->image, 'categories');
        }

        $this->delete($category->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted category!'
        ]);
    }
}
