<?php

namespace App\Repositories;

use App\Http\Resources\Brands;
use App\Traits\UploadTrait;
use Illuminate\Http\UploadedFile;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BrandRepository;
use App\Models\Brand;
use App\Validators\BrandValidator;

/**
 * Class BrandRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BrandRepositoryEloquent extends BaseRepository implements BrandRepository
{
    use UploadTrait;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Brand::class;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param string $order
     * @param string $sort
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function listBrands(string $order = 'id', string $sort = 'desc')
    {
        // TODO: Implement listBrands() method.
        $listBrands = $this->model->orderBy($order, $sort)->get();

        return Brands::collection($listBrands);
    }

    /**
     * @param int $id
     * @return Brands|mixed
     */
    public function findBrandById(int $id)
    {
        // TODO: Implement findBrandById() method.
        $brand = $this->model->findOrFail($id);

        return new Brands($brand);
    }

    /**
     * @param array $params
     * @return Brands|mixed
     */
    public function createBrand(array $params)
    {
        // TODO: Implement createBrand() method.
        $collection = collect($params);

        $logo = null;

        if ($collection->has('logo') && ($params['logo'] instanceof UploadedFile)) {
            $logo = $this->uploadFile($params['logo'], 'brands');
        }

        $merge = $collection->merge(compact('logo'));

        $brand = new Brand($merge->all());

        $brand->save();

        return new Brands($brand);
    }

    /**
     * @param array $params
     * @return Brands|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateBrand(array $params)
    {
        // TODO: Implement updateBrand() method.
        $brand = $this->findBrandById($params['id']);

        $collection = collect($params);

        if ($collection->has('logo') && ($params['logo'] instanceof UploadedFile)) {
            if ($brand->logo != null) {
                $this->unlinkFile($brand->logo, 'brands');
            }

            $logo = $this->uploadFile($params['logo'], 'brands');
        }

        $merge = $collection->merge(compact('logo'));

        $brand = $this->update($merge->all(), $brand->id);

        return new Brands($brand);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function deleteBrand($id)
    {
        // TODO: Implement deleteBrand() method.
        $brand = $this->findBrandById($id);

        if ($brand->logo != null) {
            $this->unlinkFile($brand->logo, 'brands');
        }

        $this->delete($brand->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted brand!'
        ]);
    }
}
