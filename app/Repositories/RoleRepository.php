<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository.
 *
 * @package namespace App\Repositories;
 */
interface RoleRepository extends RepositoryInterface
{
    /**
     * @param string $order
     * @param string $sort
     * @return mixed
     */
    public function roles(string $order = 'id', string $sort = 'desc');

    /**
     * @param int $id
     * @return mixed
     */
    public function findRoleById(int $id);

    /**
     * @param array $params
     * @return mixed
     */
    public function createRole(array $params);

    /**
     * @param array $params
     * @return mixed
     */
    public function updateRole(array $params);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRole($id);
}
