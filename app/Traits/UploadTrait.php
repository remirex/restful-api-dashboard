<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

trait UploadTrait
{
    public function uploadFile(UploadedFile $uploadedFile, $folder = null)
    {
        // Define folder path
        $destinationPath = public_path('uploads/' . $folder . '/');
        // Define thumbnail folder path
        $thumbnailPath = $destinationPath . 'thumbnail/';
        // Define file extension
        $extension = $uploadedFile->getClientOriginalExtension();
        // Make a image name based on current date
        $name = date('YmdHis') . '.' . $extension;

        // Check if directory exist
        if (!is_dir($destinationPath) or !is_dir($thumbnailPath)) {
            mkdir($destinationPath);
            mkdir($thumbnailPath);
            chmod($destinationPath, 0755);
            chmod($thumbnailPath, 0755);
        }

        // Make uploaded file
        $thumbnailImage = Image::make($uploadedFile);
        // save original image
        $thumbnailImage->save($destinationPath.$name);
        // save thumbnail image
        $thumbnailImage->resize(150, 150);
        $thumbnailImage->save($thumbnailPath.$name);

        return $name;
    }

    public function unlinkFile($filename = null, $folder = null)
    {
        // Define file in folder path
        $fileOriginal = public_path('uploads/' . $folder . '/' . $filename);
        // Define file in thumbnail path
        $fileThumbnail = public_path('uploads/' . $folder . '/thumbnail/' .$filename);
        // Delete file from folder
        if (File::exists($fileOriginal) and File::exists($fileThumbnail)) {
            unlink($fileOriginal);
            unlink($fileThumbnail);
        }
    }
}