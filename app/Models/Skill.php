<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Skill.
 *
 * @package namespace App\Models;
 */
class Skill extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'skills';

    protected $fillable = ['name'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'skill_user', 'skill_id', 'user_id');
    }

}
