<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Brands extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'brand_id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'logo' => config('app.url') . '/uploads/brands/' . $this->logo,
            'thumbnail_logo' => config('app.url') . '/uploads/brands/thumbnail/' . $this->logo,
        ];
    }
}
