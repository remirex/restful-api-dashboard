<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Categories extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'category_id' => $this->id,
            'parent_id' => $this->parent_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'featured' => $this->featured,
            'menu' => $this->menu,
            'image' => config('app.url') . '/uploads/categories/' . $this->image,
            'thumbnail' => config('app.url') . '/uploads/categories/thumbnail/' . $this->image,
        ];
    }
}
