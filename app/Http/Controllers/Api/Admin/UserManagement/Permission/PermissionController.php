<?php

namespace App\Http\Controllers\Api\Admin\UserManagement\Permission;

use App\Http\Controllers\Controller;
use App\Repositories\PermissionRepositoryEloquent;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * @var PermissionRepositoryEloquent
     */
    protected $permissionRepository;

    /**
     * PermissionController constructor.
     * @param PermissionRepositoryEloquent $permissionRepository
     */
    public function __construct(PermissionRepositoryEloquent $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function all()
    {
        return $this->permissionRepository->permissions();
    }

    /**
     * @param int $id
     * @return \App\Http\Resources\Permissions|mixed
     */
    public function find(int $id)
    {
        return $this->permissionRepository->findPermissionById($id);
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Permissions|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $params = $request->all();

        $permission = $this->permissionRepository->createPermission($params);

        return $permission;
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Permissions|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $params = $request->all();

        $permission = $this->permissionRepository->updatePermission($params);

        return $permission;
    }

    /**
     * @param $permissionId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($permissionId)
    {
        return $this->permissionRepository->deletePermission($permissionId);
    }
}
