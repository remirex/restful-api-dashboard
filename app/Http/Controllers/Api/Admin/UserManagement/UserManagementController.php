<?php

namespace App\Http\Controllers\Api\Admin\UserManagement;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    /**
     * @var UserRepositoryEloquent
     */
    protected $userRepository;

    /**
     * UserManagementController constructor.
     * @param UserRepositoryEloquent $userRepository
     */
    public function __construct(UserRepositoryEloquent $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        return $this->userRepository->listUsers();
    }
}
