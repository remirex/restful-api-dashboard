<?php

namespace App\Http\Controllers\Api\Admin\UserManagement\Role;

use App\Http\Controllers\Controller;
use App\Repositories\RoleRepositoryEloquent;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * @var RoleRepositoryEloquent
     */
    protected $roleRepository;

    /**
     * RoleController constructor.
     * @param RoleRepositoryEloquent $roleRepository
     */
    public function __construct(RoleRepositoryEloquent $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function all()
    {
        return $this->roleRepository->roles();
    }

    /**
     * @param int $id
     * @return \App\Http\Resources\Roles|mixed
     */
    public function find(int $id)
    {
        return $this->roleRepository->findRoleById($id);
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Roles|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'name' => 'required'
        ]);

        $role = $this->roleRepository->createRole($params);

        return $role;
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Roles|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'name' => 'required'
        ]);

        $role = $this->roleRepository->updateRole($params);

        return $role;
    }

    /**
     * @param $roleId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($roleId)
    {
        return $this->roleRepository->deleteRole($roleId);
    }
}
