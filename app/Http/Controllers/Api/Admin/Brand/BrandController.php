<?php

namespace App\Http\Controllers\Api\Admin\Brand;

use App\Http\Controllers\Controller;
use App\Repositories\BrandRepositoryEloquent;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * @var BrandRepositoryEloquent
     */
    protected $brandRepository;

    /**
     * BrandController constructor.
     * @param BrandRepositoryEloquent $brandRepository
     */
    public function __construct(BrandRepositoryEloquent $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function all()
    {
        return $this->brandRepository->listBrands();
    }

    /**
     * @param int $id
     * @return \App\Http\Resources\Brands|mixed
     */
    public function find(int $id)
    {
        return $this->brandRepository->findBrandById($id);
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Brands|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'logo' => 'mimes:jpg,jpeg,png|max:1000'
        ]);

        $brand = $this->brandRepository->createBrand($params);

        return $brand;
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Brands|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'logo' => 'mimes:jpg,jpeg,png|max:1000'
        ]);

        $brand = $this->brandRepository->updateBrand($params);

        return $brand;
    }

    /**
     * @param $brandId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($brandId)
    {
        return $this->brandRepository->deleteBrand($brandId);
    }
}
