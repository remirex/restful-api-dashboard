<?php

namespace App\Http\Controllers\Api\Admin\Category;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryEloquent
     */
    protected $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepositoryEloquent $categoryRepository
     */
    public function __construct(CategoryRepositoryEloquent $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->middleware('permission:delete article', ['only' => ['delete']]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function all()
    {
        return $this->categoryRepository->listCategories();
    }

    /**
     * @param $categoryId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function find(int $categoryId)
    {
        return $this->categoryRepository->findCategoryById($categoryId);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories',
            'parent_id' => 'required|not_in:0',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $params = $request->all();

        return $this->categoryRepository->createCategory($params);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'parent_id' => 'required|not_in:0',
            'image' => 'required'
        ]);

        return $this->categoryRepository->updateCategory($params);
    }

    /**
     * @param $categoryId
     * @return \Illuminate\Http\JsonResponse|int|mixed
     */
    public function delete($categoryId)
    {
        return $this->categoryRepository->deleteCategory($categoryId);
    }
}
