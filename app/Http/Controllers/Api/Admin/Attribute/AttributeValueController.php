<?php

namespace App\Http\Controllers\Api\Admin\Attribute;

use App\Http\Controllers\Controller;
use App\Models\AttributeValue;
use App\Repositories\AttributeRepositoryEloquent;
use Illuminate\Http\Request;

class AttributeValueController extends Controller
{
    /**
     * @var AttributeRepositoryEloquent
     */
    protected $attributeRepository;

    /**
     * AttributeValueController constructor.
     * @param AttributeRepositoryEloquent $attributeRepository
     */
    public function __construct(AttributeRepositoryEloquent $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getValues(Request $request)
    {
        $attributeId = $request->input('id');

        $attribute = $this->attributeRepository->findAttributeById($attributeId);

        $values = $attribute->values;

        return response()->json($values);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addValues(Request $request)
    {
        $value = new AttributeValue();

        $value->attribute_id = $request->input('id');
        $value->value = $request->input('value');
        $value->price = $request->input('price');

        $value->save();

        return response()->json($value);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateValues(Request $request)
    {
        $attributeValue = AttributeValue::findOrFail($request->input('valueId'));

        $attributeValue->attribute_id = $request->input('id');
        $attributeValue->value = $request->input('value');
        $attributeValue->price = $request->input('price');

        $attributeValue->save();

        return response()->json($attributeValue);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteValues(Request $request)
    {
        $attributeValue = AttributeValue::findOrFail($request->input('id'));
        $attributeValue->delete();

        return response()->json([
            'status' => 'success',
            'messages' => 'Attribute value deleted successfully.'
        ]);
    }
}
