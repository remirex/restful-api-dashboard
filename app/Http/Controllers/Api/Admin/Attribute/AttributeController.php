<?php

namespace App\Http\Controllers\Api\Admin\Attribute;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Repositories\AttributeRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AttributeController extends Controller
{
    /**
     * @var AttributeRepositoryEloquent
     */
    protected $attributeRepository;

    /**
     * AttributeController constructor.
     * @param AttributeRepositoryEloquent $attributeRepository
     */
    public function __construct(AttributeRepositoryEloquent $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|mixed
     */
    public function all()
    {
        return $this->attributeRepository->listAttributes();
    }

    /**
     * @param int $attributeId
     * @return \App\Http\Resources\Attributes|mixed
     */
    public function find(int $attributeId)
    {
        return $this->attributeRepository->findAttribute($attributeId);
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Attributes|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:attributes',
            'name' => 'required',
            'frontend_type' => 'required'
        ]);

        $params = $request->all();

        $attributes = $this->attributeRepository->createAttribute($params);

        return $attributes;
    }

    /**
     * @param Request $request
     * @return \App\Http\Resources\Attributes|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $params = $request->all();

        $this->validate($request, [
            'code' => ['required', Rule::unique('attributes', 'code')->ignore($params['id'])],
            'name' => 'required',
            'frontend_type' => 'required'
        ]);

        $attribute = $this->attributeRepository->updateAttribute($params);

        return $attribute;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($id)
    {
        return $this->attributeRepository->deleteAttribute($id);
    }
}
