<?php

namespace App\Http\Controllers\Api\Skill;

use App\Http\Controllers\Controller;
use App\Repositories\SkillRepositoryEloquent;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    protected $skillRepository;

    public function __construct(SkillRepositoryEloquent $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    public function skills()
    {
        return $this->skillRepository->skills();
    }
}
