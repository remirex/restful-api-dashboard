<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    Route::get('user', 'Api\User\UserController@user');
    Route::get('logout', 'Api\AuthController@logout');


});

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');

    Route::post('password/email', 'Api\Password\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Api\Password\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Api\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Api\VerificationController@resend');

    Route::get('oauth/{service}', 'Api\OAuthController@redirectToProvider');
    Route::get('oauth/{service}/callback', 'Api\OAuthController@handleProviderCallback');

    Route::get('skills', 'Api\Skill\SkillController@skills');
});
// Categories
Route::group(['prefix'  =>   'v1/categories', 'middleware' => 'auth:api'], function() {

    Route::get('/', 'Api\Admin\Category\CategoryController@all')->name('admin.categories.all');
    Route::get('/{categoryId}', 'Api\Admin\Category\CategoryController@find')->name('admin.categories.find');
    Route::post('/store', 'Api\Admin\Category\CategoryController@store')->name('admin.categories.store');
    Route::put('/update', 'Api\Admin\Category\CategoryController@update')->name('admin.categories.update');
    Route::get('/{id}/delete', 'Api\Admin\Category\CategoryController@delete')->name('admin.categories.delete');

});
// Attributes
Route::group(['prefix'  =>   'v1/attributes'], function() {

    Route::get('/', 'Api\Admin\Attribute\AttributeController@all')->name('admin.attributes.all');
    Route::get('/{attributeId}', 'Api\Admin\Attribute\AttributeController@find')->name('admin.attributes.find');
    Route::post('/store', 'Api\Admin\Attribute\AttributeController@store')->name('admin.attributes.store');
    Route::post('/update', 'Api\Admin\Attribute\AttributeController@update')->name('admin.attributes.update');
    Route::get('/{id}/delete', 'Api\Admin\Attribute\AttributeController@delete')->name('admin.attributes.delete');
    // Attribute value
    Route::post('/get-values', 'Api\Admin\Attribute\AttributeValueController@getValues');
    Route::post('/add-values', 'Api\Admin\Attribute\AttributeValueController@addValues');
    Route::post('/update-values', 'Api\Admin\Attribute\AttributeValueController@updateValues');
    Route::post('/delete-values', 'Api\Admin\Attribute\AttributeValueController@deleteValues');
});
// Brands
Route::group(['prefix'  =>   'v1/brands'], function() {

    Route::get('/', 'Api\Admin\Brand\BrandController@all')->name('admin.brands.all');
    Route::get('/{brandId}', 'Api\Admin\Brand\BrandController@find')->name('admin.brands.find');
    Route::post('/store', 'Api\Admin\Brand\BrandController@store')->name('admin.brands.store');
    Route::post('/update', 'Api\Admin\Brand\BrandController@update')->name('admin.brands.update');
    Route::get('/{id}/delete', 'Api\Admin\Brand\BrandController@delete')->name('admin.brands.delete');

});
// Role
Route::group(['prefix'  =>   'v1/roles'], function() {

    Route::get('/', 'Api\Admin\UserManagement\Role\RoleController@all');
    Route::get('/{roleId}', 'Api\Admin\UserManagement\Role\RoleController@find');
    Route::post('/store', 'Api\Admin\UserManagement\Role\RoleController@store');
    Route::put('/update', 'Api\Admin\UserManagement\Role\RoleController@update');
    Route::get('/{roleId}/delete', 'Api\Admin\UserManagement\Role\RoleController@delete');

});
// Permission
Route::group(['prefix'  =>   'v1/permissions'], function() {

    Route::get('/', 'Api\Admin\UserManagement\Permission\PermissionController@all');
    Route::get('/{permissionId}', 'Api\Admin\UserManagement\Permission\PermissionController@find');
    Route::post('/store', 'Api\Admin\UserManagement\Permission\PermissionController@store');
    Route::put('/update', 'Api\Admin\UserManagement\Permission\PermissionController@update');
    Route::get('/{permissionId}/delete', 'Api\Admin\UserManagement\Permission\PermissionController@delete');

});


Route::get('users', 'Api\Admin\UserManagement\UserManagementController@all');

